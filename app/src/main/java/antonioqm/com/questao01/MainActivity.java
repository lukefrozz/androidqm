package antonioqm.com.questao01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void questao01(View view){
//        registra minha itenção informando onde estou e para onde vou
        Intent i = new Intent(this, Questao01.class);
//        inicia minha activity setada
        startActivity(i);
    }
}
