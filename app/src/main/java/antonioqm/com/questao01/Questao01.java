package antonioqm.com.questao01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Questao01 extends AppCompatActivity {

    private EditText valPrimo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questao01);
//        captura o valor que vem do campo text da View em cursos
        valPrimo = (EditText) findViewById(R.id.editTextPrimo);
    }

    public void calculaPrimo(View view){
        double val = Double.parseDouble(valPrimo.getText().toString());

        int i = 1, v = 0;

        while (i <= val){
            if (val % i == 0){
                v++;
            }
            i++;
        }

        TextView result = (TextView) findViewById(R.id.textPrimo);

        if (v == 2){
            result.setText("Número Primo!");
        }else{
            result.setText("O número não é Primo!");
        }

    }
}
